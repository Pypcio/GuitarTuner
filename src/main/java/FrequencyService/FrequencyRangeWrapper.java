package FrequencyService;

import java.util.Optional;

import static java.util.Optional.of;

/**
 * Created by Szymon Pypłacz on 2017-05-02.
 */
public class FrequencyRangeWrapper {

    private float minFrequency;
    private float idealFrequency;
    private float maxFrequency;

    public FrequencyRangeWrapper(float minFrequency, float maxFrequency, float idealFrequency) {
        this.minFrequency = minFrequency;
        this.idealFrequency = idealFrequency;
        this.maxFrequency = maxFrequency;
    }

    public float getIdealFrequency() {
        return idealFrequency;
    }

    public Optional<Integer> setDeviationLevel(float frequency) {
        float percent;
        if (frequency < idealFrequency) {
            float deviationRangePercent = (idealFrequency - minFrequency) / 100;
            float difference = idealFrequency - frequency;
            percent = -difference / deviationRangePercent;
        } else {
            float deviationRangePercent = (maxFrequency - idealFrequency) / 100;
            float difference = frequency - idealFrequency;
            percent = difference / deviationRangePercent;
        }
        if (percent < -70) {
            return of(-3);
        } else if (percent < -40) {
            return of(-2);
        } else if (percent < -10) {
            return of(-1);
        } else if (percent < 10) {
            return of(0);
        } else if (percent < 40) {
            return of(1);
        } else if (percent < 70) {
            return of(2);
        } else if (percent < 100) {
            return of(3);
        } else {
            return Optional.empty();
        }
    }
}
