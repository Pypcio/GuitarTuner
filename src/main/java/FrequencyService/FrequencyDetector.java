package FrequencyService;

import AudioService.WaveFileAudio;
import org.apache.log4j.Logger;
import org.jtransforms.fft.DoubleFFT_1D;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Optional;

/**
 * Created by Szymon Pypłacz on 2017-04-22.
 */
public class FrequencyDetector {

    private final static String FILE_NAME = "audio.wav";
    static Logger log = Logger.getLogger(FrequencyDetector.class.getName());

    public static Optional<Double> getFrequency() {
        float freq = 0;
        WaveFileAudio waveFileAudio = null;
        try {
            waveFileAudio = new WaveFileAudio(new File(FILE_NAME));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Optional<Integer> multiply = Optional.ofNullable(waveFileAudio.getSampleRateInHz() * waveFileAudio.getChannels());
        if (multiply.isPresent()) {
            if (waveFileAudio.getSamples().length > 0) {
                int bins = waveFileAudio.getSamples().length;
                double fftResult[] = waveFileAudio.getSamples();
                DoubleFFT_1D fft1Da = new DoubleFFT_1D(bins / 2);
                fft1Da.complexForward(fftResult);

                double[] spectrum = new double[bins];
                for (int i = 0; i < bins / 2; i++) {
                    double re = fftResult[2 * i];
                    double im = fftResult[(2 * i) + 1];
                    spectrum[i] = Math.sqrt((re * re) + (im * im));
                }
                double max = Arrays.stream(Arrays.copyOf(spectrum, (bins - 1) / 4)).max().getAsDouble();
                for (int i = 0; i < bins / 2; i++) {
                    if (spectrum[i] == max) {
                        if (freq == 0) {
                            freq = i * (float) multiply.get() / (bins - 1);
                            log.debug("Calculated frequency: " + freq);
                        }
                    }
                }

                return Optional.of(BigDecimal.valueOf(freq)
                        .setScale(2, RoundingMode.HALF_UP)
                        .doubleValue());
            } else {
                return Optional.empty();
            }
        }
        return Optional.empty();

    }
}
                                              