package FrequencyService;

import java.util.NavigableMap;
import java.util.Optional;
import java.util.TreeMap;

import static java.lang.Float.valueOf;

/**
 * Created by Szymon Pypłacz on 2017-05-02.
 */
public class FrequencyToNoteRangeDictionary {
    NavigableMap<Float, String> noteMap = new TreeMap<>();
    TreeMap<String, FrequencyRangeWrapper> noteRangeMap = new TreeMap<>();

    public FrequencyToNoteRangeDictionary() {
        //1
        noteMap.put(valueOf(33.65f), "C1");
        noteMap.put(valueOf(35.65f), "C#1");
        noteMap.put(valueOf(37.8f), "D1");
        noteMap.put(valueOf(40.05f), "D#1");
        noteMap.put(valueOf(42.45f), "E1");
        noteMap.put(valueOf(44.95f), "F1");
        noteMap.put(valueOf(47.6f), "F#1");
        noteMap.put(valueOf(50.45f), "G1");
        noteMap.put(valueOf(53.45f), "G#1");
        noteMap.put(valueOf(56.65f), "A1");
        noteMap.put(valueOf(60f), "A#1");
        noteMap.put(valueOf(63.55f), "B1");

        //2
        noteMap.put(valueOf(67.35f), "C2");
        noteMap.put(valueOf(71.35f), "C#2");
        noteMap.put(valueOf(75.6f), "D2");
        noteMap.put(valueOf(80.1f), "D#2");
        noteMap.put(valueOf(84.85f), "E2");
        noteMap.put(valueOf(89.9f), "F2");
        noteMap.put(valueOf(95.25f), "F#2");
        noteMap.put(valueOf(100.9f), "G2");
        noteMap.put(valueOf(106.9f), "G#2");
        noteMap.put(valueOf(113.25f), "A2");
        noteMap.put(valueOf(120f), "A#2");
        noteMap.put(valueOf(127.15f), "B2");

        //3
        noteMap.put(valueOf(134.7f), "C3");
        noteMap.put(valueOf(142.7f), "C#3");
        noteMap.put(valueOf(151.2f), "D3");
        noteMap.put(valueOf(160.2f), "D#3");
        noteMap.put(valueOf(169.7f), "E3");
        noteMap.put(valueOf(179.8f), "F3");
        noteMap.put(valueOf(190.5f), "F#3");
        noteMap.put(valueOf(201.85f), "G3");
        noteMap.put(valueOf(213.85f), "G#3");
        noteMap.put(valueOf(226.55f), "A3");
        noteMap.put(valueOf(240f), "A#3");
        noteMap.put(valueOf(254.25f), "B3");

        //4
        noteMap.put(valueOf(269.4f), "C4");
        noteMap.put(valueOf(285.45f), "C#4");
        noteMap.put(valueOf(302.4f), "D4");
        noteMap.put(valueOf(320.35f), "D#4");
        noteMap.put(valueOf(339.4f), "E4");
        noteMap.put(valueOf(359.6f), "F4");
        noteMap.put(valueOf(381), "F#4");
        noteMap.put(valueOf(403.65f), "G4");
        noteMap.put(valueOf(427.65f), "G#4");
        noteMap.put(valueOf(453.1f), "A4");
        noteMap.put(valueOf(480.05f), "A#4");
        noteMap.put(valueOf(508.6f), "B4");

        //5
        noteMap.put(valueOf(538.85f), "C5");
        noteMap.put(valueOf(570.85f), "C#5");
        noteMap.put(valueOf(604.8f), "D5");
        noteMap.put(valueOf(640.4f), "D#5");
        noteMap.put(valueOf(678.5f), "E5");
        noteMap.put(valueOf(719.25f), "F5");
        noteMap.put(valueOf(762), "F#5");
        noteMap.put(valueOf(807.3f), "G5");
        noteMap.put(valueOf(855.3f), "G#5");
        noteMap.put(valueOf(906.15f), "A5");
        noteMap.put(valueOf(955.55f), "A#5");
        noteMap.put(valueOf(1012.65f), "B5");

        //6
        noteMap.put(valueOf(1177.6f), "C6");
        noteMap.put(valueOf(1141.7f), "C#6");
        noteMap.put(valueOf(1209.6f), "D6");
        noteMap.put(valueOf(1281.5f), "D#6");
        noteMap.put(valueOf(1357.7f), "E6");
        noteMap.put(valueOf(1438.45f), "F6");
        noteMap.put(valueOf(1524), "F#6");
        noteMap.put(valueOf(1614.6f), "G6");
        noteMap.put(valueOf(1710.6f), "G#6");
        noteMap.put(valueOf(1812.35f), "A6");
        noteMap.put(valueOf(1920.1f), "A#6");
        noteMap.put(valueOf(2034.25f), "B6");

        noteMap.put(valueOf(2155.25f), "C7");
        noteMap.put(valueOf(2283.4f), "C#7");
        noteMap.put(valueOf(2419.15f), "D7");
        noteMap.put(valueOf(2563f), "D#7");
        noteMap.put(valueOf(2715.4f), "E7");
        noteMap.put(valueOf(2876.9f), "F7");
        noteMap.put(valueOf(3048f), "F#7");
        noteMap.put(valueOf(3229.2f), "G7");
        noteMap.put(valueOf(3421.2f), "G#7");
        noteMap.put(valueOf(3624.65f), "A7");
        noteMap.put(valueOf(3840.2f), "A#7");
        noteMap.put(valueOf(4062f), "B7");


        noteRangeMap.put("C1", new FrequencyRangeWrapper(31.75f, 33.65f, 32.7f));
        noteRangeMap.put("C#1", new FrequencyRangeWrapper(33.65f, 35.65f, 34.6f));
        noteRangeMap.put("D1", new FrequencyRangeWrapper(35.65f, 37.8f, 36.7f));
        noteRangeMap.put("D#1", new FrequencyRangeWrapper(37.8f, 40.05f, 38.9f));
        noteRangeMap.put("E1", new FrequencyRangeWrapper(40.05f, 42.45f, 41.2f));
        noteRangeMap.put("F1", new FrequencyRangeWrapper(42.45f, 44.95f, 43.7f));
        noteRangeMap.put("F#1", new FrequencyRangeWrapper(44.95f, 47.6f, 46.2f));
        noteRangeMap.put("G1", new FrequencyRangeWrapper(47.6f, 50.45f, 49));
        noteRangeMap.put("G#1", new FrequencyRangeWrapper(50.45f, 53.45f, 51.9f));
        noteRangeMap.put("A1", new FrequencyRangeWrapper(53.45f, 56.65f, 55));
        noteRangeMap.put("A#1", new FrequencyRangeWrapper(56.65f, 60f, 58.3f));
        noteRangeMap.put("B1", new FrequencyRangeWrapper(60f, 63.55f, 65.4f));
        noteRangeMap.put("C2", new FrequencyRangeWrapper(63.55f, 67.35f, 69.3f));
        noteRangeMap.put("C#2", new FrequencyRangeWrapper(67.35f, 71.35f, 69.3f));
        noteRangeMap.put("D2", new FrequencyRangeWrapper(71.35f, 75.6f, 73.4f));
        noteRangeMap.put("D#2", new FrequencyRangeWrapper(75.6f, 80.1f, 77.8f));
        noteRangeMap.put("E2", new FrequencyRangeWrapper(80.1f, 84.85f, 82.4f));
        noteRangeMap.put("F2", new FrequencyRangeWrapper(84.85f, 89.9f, 87.3f));
        noteRangeMap.put("F#2", new FrequencyRangeWrapper(89.9f, 95.25f, 92.5f));
        noteRangeMap.put("G2", new FrequencyRangeWrapper(95.25f, 100.9f, 98f));
        noteRangeMap.put("G#2", new FrequencyRangeWrapper(100.9f, 106.9f, 103.8f));
        noteRangeMap.put("A2", new FrequencyRangeWrapper(106.9f, 113.25f, 110f));
        noteRangeMap.put("A#2", new FrequencyRangeWrapper(113.25f, 120f, 116.5f));
        noteRangeMap.put("B2", new FrequencyRangeWrapper(120f, 127.15f, 123.5f));
        noteRangeMap.put("C3", new FrequencyRangeWrapper(127.15f, 134.7f, 130.8f));
        noteRangeMap.put("C#3", new FrequencyRangeWrapper(134.7f, 142.7f, 138.6f));
        noteRangeMap.put("D3", new FrequencyRangeWrapper(142.7f, 151.2f, 146.8f));
        noteRangeMap.put("D#3", new FrequencyRangeWrapper(151.2f, 160.2f, 155.6f));
        noteRangeMap.put("E3", new FrequencyRangeWrapper(160.2f, 169.7f, 164.8f));
        noteRangeMap.put("F3", new FrequencyRangeWrapper(169.7f, 179.8f, 174.6f));
        noteRangeMap.put("F#3", new FrequencyRangeWrapper(179.8f, 190.5f, 185));
        noteRangeMap.put("G3", new FrequencyRangeWrapper(190.5f, 201.85f, 196));
        noteRangeMap.put("G#3", new FrequencyRangeWrapper(201.85f, 213.85f, 207.7f));
        noteRangeMap.put("A3", new FrequencyRangeWrapper(213.85f, 226.55f, 220));
        noteRangeMap.put("A#3", new FrequencyRangeWrapper(226.55f, 240f, 233.1f));
        noteRangeMap.put("B3", new FrequencyRangeWrapper(240f, 254.25f, 246.9f));
        noteRangeMap.put("C4", new FrequencyRangeWrapper(254.25f, 269.4f, 261.6f));
        noteRangeMap.put("C#4", new FrequencyRangeWrapper(269.4f, 285.45f, 277.f));
        noteRangeMap.put("D4", new FrequencyRangeWrapper(285.45f, 302.4f, 293.7f));
        noteRangeMap.put("D#4", new FrequencyRangeWrapper(302.4f, 320.35f, 311.1f));
        noteRangeMap.put("E4", new FrequencyRangeWrapper(320.35f, 339.4f, 329.6f));
        noteRangeMap.put("F4", new FrequencyRangeWrapper(339.4f, 359.6f, 349.2f));
        noteRangeMap.put("F#4", new FrequencyRangeWrapper(359.6f, 381, 370));
        noteRangeMap.put("G4", new FrequencyRangeWrapper(381, 403.65f, 392));
        noteRangeMap.put("G#4", new FrequencyRangeWrapper(403.65f, 427.65f, 415.3f));
        noteRangeMap.put("A4", new FrequencyRangeWrapper(427.65f, 453.1f, 440));
        noteRangeMap.put("A#4", new FrequencyRangeWrapper(453.1f, 480.05f, 466.2f));
        noteRangeMap.put("B4", new FrequencyRangeWrapper(480.05f, 508.6f, 493.9f));
        noteRangeMap.put("C5", new FrequencyRangeWrapper(508.6f, 538.85f, 523.3f));
        noteRangeMap.put("C#5", new FrequencyRangeWrapper(538.85f, 570.85f, 554.4f));
        noteRangeMap.put("D5", new FrequencyRangeWrapper(570.85f, 604.8f, 587.3f));
        noteRangeMap.put("D#5", new FrequencyRangeWrapper(604.8f, 640.4f, 622.3f));
        noteRangeMap.put("E5", new FrequencyRangeWrapper(640.4f, 678.5f, 658.5f));
        noteRangeMap.put("F5", new FrequencyRangeWrapper(678.5f, 719.25f, 698.5f));
        noteRangeMap.put("F#5", new FrequencyRangeWrapper(719.25f, 762, 740));
        noteRangeMap.put("G5", new FrequencyRangeWrapper(762, 807.3f, 784));
        noteRangeMap.put("G#5", new FrequencyRangeWrapper(807.3f, 855.3f, 830.6f));
        noteRangeMap.put("A5", new FrequencyRangeWrapper(855.3f, 906.15f, 880));
        noteRangeMap.put("A#5", new FrequencyRangeWrapper(906.15f, 955.55f, 932.3f));
        noteRangeMap.put("B5", new FrequencyRangeWrapper(955.55f, 1012.65f, 978.8f));
        noteRangeMap.put("C6", new FrequencyRangeWrapper(1012.65f, 1177.6f, 1046.5f));
        noteRangeMap.put("C#6", new FrequencyRangeWrapper(1177.6f, 1141.7f, 1108.7f));
        noteRangeMap.put("D6", new FrequencyRangeWrapper(1141.7f, 1209.6f, 1174.7f));
        noteRangeMap.put("D#6", new FrequencyRangeWrapper(1209.6f, 1281.5f, 1244.5f));
        noteRangeMap.put("E6", new FrequencyRangeWrapper(1281.5f, 1357.7f, 1318.5f));
        noteRangeMap.put("F6", new FrequencyRangeWrapper(1357.7f, 1438.45f, 1396.9f));
        noteRangeMap.put("F#6", new FrequencyRangeWrapper(1438.45f, 1524, 1480));
        noteRangeMap.put("G6", new FrequencyRangeWrapper(1524, 1614.6f, 1568));
        noteRangeMap.put("G#6", new FrequencyRangeWrapper(1614.6f, 1710.6f, 1661.2f));
        noteRangeMap.put("A6", new FrequencyRangeWrapper(1710.6f, 1812.35f, 1760));
        noteRangeMap.put("A#6", new FrequencyRangeWrapper(1812.35f, 1920.1f, 1864.7f));
        noteRangeMap.put("B6", new FrequencyRangeWrapper(1920.1f, 2034.25f, 1975.5f));
        noteRangeMap.put("C7", new FrequencyRangeWrapper(2034.25f, 2155.25f, 2093f));
        noteRangeMap.put("C#7", new FrequencyRangeWrapper(2155.25f, 2283.4f, 2217.5f));
        noteRangeMap.put("D7", new FrequencyRangeWrapper(2283.4f, 2419.15f, 2349.3f));
        noteRangeMap.put("D#7", new FrequencyRangeWrapper(2419.15f, 2563, 2489));
        noteRangeMap.put("E7", new FrequencyRangeWrapper(2563, 2715.4f, 2637));
        noteRangeMap.put("F7", new FrequencyRangeWrapper(2715.4f, 2876.9f, 2793.8f));
        noteRangeMap.put("F#7", new FrequencyRangeWrapper(2876.9f, 3048, 2960));
        noteRangeMap.put("G7", new FrequencyRangeWrapper(3048, 3229.2f, 3136));
        noteRangeMap.put("G#7", new FrequencyRangeWrapper(3229.2f, 3421.2f, 3322.4f));
        noteRangeMap.put("A7", new FrequencyRangeWrapper(3421.2f, 3624.65f, 3520));
        noteRangeMap.put("A#7", new FrequencyRangeWrapper(3624.65f, 3840.2f, 3729.3f));
        noteRangeMap.put("B7", new FrequencyRangeWrapper(3840.2f, 4062, 3951.1f));
    }

    public String getNote(float freq) {
        return noteMap.get(noteMap.ceilingKey(freq));
    }

    public Optional<Integer> setDeviationLevel(String note, float freq) {
        if (freq < 31.75 || freq > 4062) {
            return Optional.empty();
        }
        FrequencyRangeWrapper range = noteRangeMap.get(note);
        Optional<Integer> deviationLevel = range.setDeviationLevel(freq);
        return deviationLevel;
    }

    public float idealFreqForNote(String note) {
        return noteRangeMap.get(note).getIdealFrequency();
    }
}
