package Enums;

/**
 * Created by Szymon Pypłacz on 2017-05-02.
 */
public enum RecordingType { JACK, MICROPHONE, NONE}
