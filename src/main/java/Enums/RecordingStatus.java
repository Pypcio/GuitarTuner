package Enums;

/**
 * Created by Szymon Pypłacz on 2017-12-05.
 */
public enum RecordingStatus { READY, RECORDING, SUCCESSED
}
