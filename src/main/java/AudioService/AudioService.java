package AudioService;

import Controller.EffectConfigurator;
import Enums.RecordingStatus;
import Enums.WorkingType;
import FrequencyService.FrequencyDetector;
import org.apache.log4j.Logger;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

/**
 * Created by Szymon Pypłacz on 2017-10-18.
 */
public class AudioService {
    static Logger log = Logger.getLogger(AudioService.class.getName());
    private static final long RECORD_TIME = 1000;
    private static final String FILENAME = "audio.wav";
    private static final WorkingType WORKING_TYPE = WorkingType.PC;
    private static final File WAV_FILE = new File(FILENAME);
    private static final AudioFileFormat.Type FILE_TYPE = AudioFileFormat.Type.WAVE;
    private static TargetDataLine targetDataLine;
    private static SourceDataLine sourceDataLine;
    private static AudioFormat format;
    private static AudioInputStream ais;
    private static EffectConfigurator configurator;

    Thread recording = new Thread(() -> {
        try {
            recordInClass();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    });

    Thread monitorThread = new Thread(() -> {
        stopRecording();
        try {
            targetDataLine.open();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
        targetDataLine.start();
        sourceDataLine.start();
        int bufferLen = 2 * 4;
        int readByte;
        byte[] data = new byte[bufferLen];
        while (!configurator.isTuner()) {
            readByte = targetDataLine.read(data, 0, bufferLen);

            clean(data);
            if (configurator.isEffect()) {
                gain(data);
                distortion(data);
            }
            sourceDataLine.write(data, 0, readByte);
        }
        sourceDataLine.stop();
        targetDataLine.stop();
    });

    public AudioService(EffectConfigurator configurator) throws LineUnavailableException {
        format = getAudioFormat(WORKING_TYPE);
        DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
        sourceDataLine = (SourceDataLine) AudioSystem.getLine(info);
        targetDataLine = getTargetDataLine(WORKING_TYPE);

        sourceDataLine.open();
        targetDataLine.open();

        this.configurator = configurator;
    }

    public void liveMonitor() throws InterruptedException {
        log.info("Initialization tuner");
        while (true) {
            synchronized (recording) {
                if (!configurator.isTuner()) {
                    log.info("Effect starts");
                    monitorThread.run();
                    monitorThread.suspend();
                    log.info("Effect ends");
                }
            }
        }
    }

    public void record() throws InterruptedException, IOException, LineUnavailableException {
        while (true) {
            if (configurator.isTuner() && RecordingStatus.READY.equals(configurator.getRecordingStatus())) {
                log.info("Tuner starts");
                configurator.setRecordingStatus(RecordingStatus.RECORDING);
                recording.run();
            } else {
                Thread.sleep(500);
            }
        }
    }

    private AudioFormat getAudioFormat(WorkingType workingType) {
        if (workingType.equals(WorkingType.RPI)) {
            float sampleRate = 8000;
            int sampleSizeInBits = 8;
            int channels = 1;
            boolean signed = true;
            boolean bigEndian = false;
            return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
        } else {
            float sampleRate = 8000;
            int sampleSizeInBits = 16;
            int channels = 2;
            boolean signed = true;
            boolean bigEndian = false;
            return new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
        }
    }

    private TargetDataLine getTargetDataLine(WorkingType workingType) throws LineUnavailableException {
        if (workingType.equals(WorkingType.RPI)) {
            AudioFormat format = getAudioFormat(workingType);
            DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
            Mixer.Info[] mixerInfo = AudioSystem.getMixerInfo();
            Mixer.Info finalMixerInfo = null;
            for (Mixer.Info aMixerInfo : mixerInfo) {
                if (aMixerInfo.getName().contains("U0x41e0x30d3") && aMixerInfo.getName().contains("plughw"))
                    finalMixerInfo = aMixerInfo;
            }
            Mixer mixer = AudioSystem.getMixer(finalMixerInfo);
            return (TargetDataLine) mixer.getLine(info);
        } else {
            AudioFormat format = getAudioFormat(workingType);
            DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
            return (TargetDataLine) AudioSystem.getLine(info);
        }
    }

    private void stopRecording() {
        recording.suspend();
        if (targetDataLine.isOpen()) {
            try {
                targetDataLine.flush();
                targetDataLine.close();
                targetDataLine.open();
            } catch (LineUnavailableException e) {
                e.printStackTrace();
            }
        }
    }

    private void recordInClass() throws IOException, LineUnavailableException {
        Thread stopper = new Thread(() -> {
            try {
                Thread.sleep(RECORD_TIME);
                saveFrequently();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        stopper.start();
        startRecording();
    }

    private void startRecording() throws IOException, LineUnavailableException {
        targetDataLine.open(format);
        targetDataLine.start();
        ais = new AudioInputStream(targetDataLine);
        AudioSystem.write(ais, FILE_TYPE, WAV_FILE);
    }

    private void saveFrequently() throws IOException {
        targetDataLine.stop();
        targetDataLine.close();
        Optional<Double> freq = FrequencyDetector.getFrequency();
        freq.ifPresent((x) -> configurator.setLastRecordedFreq(x));
        configurator.setRecordingStatus(RecordingStatus.SUCCESSED);
    }

    private void distortion(byte[] data) {
        for (int i = 0; i < data.length; i++) {
            if (data[i] > configurator.getDistortionLevel()) {
                data[i] = (byte) (configurator.getDistortionLevel() + configurator.getDistortionGain());
            } else if (data[i] < -configurator.getDistortionLevel()) {
                data[i] = (byte) -(configurator.getDistortionLevel() + configurator.getDistortionGain());
            }
        }
    }

    private void clean(byte[] data) {
        for (int i = 0; i < data.length; i++) {
            if (data[i] < configurator.getCleanTone() && data[i] > -configurator.getCleanTone()) {
                data[i] = 0;
            }
        }
    }

    private void gain(byte[] data) {
        for (int i = 0; i < data.length; i++) {
            if (data[i] > 5) {
                data[i] += configurator.getGain();
            } else if (data[i] < -5) {
                data[i] -= configurator.getGain();
            }
        }
    }

}
