package Controller;

import javafx.scene.paint.Color;

/**
 * Created by Szymon on 2017-03-11.
 */
public class ColorFactory {
    public final static Color GREEN_COLOR = Color.rgb(10, 218, 34);
    public final static Color RED_COLOR = Color.rgb(255, 41, 62);
    public final static Color GREY_COLOR = Color.rgb(142, 142, 142);
    public final static Color LIGHT_BLUE_COLOR = Color.rgb(178, 197, 218);
    public final static Color LIGHT_BROWN_COLOR = Color.rgb(232, 223, 199);
    public final static Color DARK_BROWN_COLOR = Color.rgb(128, 51, 00);
}