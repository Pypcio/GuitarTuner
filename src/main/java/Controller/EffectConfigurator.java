package Controller;

import Enums.RecordingStatus;

/**
 * Created by Szymon Pypłacz on 2017-11-04.
 */
public class EffectConfigurator {

    private byte distortionLevel;
    private byte distortionGain;
    private byte gain;
    private byte cleanTone = 3;
    private double lastRecordedFreq;
    private boolean effect;
    private boolean tuner;
    private RecordingStatus recordingStatus;


    public EffectConfigurator(int distortionLevel, int distortionGain, int gain) {
        this.distortionLevel = (byte) distortionLevel;
        this.distortionGain = (byte) distortionGain;
        this.gain = (byte) gain;
        tuner = false;
        recordingStatus = RecordingStatus.READY;
    }

    public boolean isTuner() {
        return tuner;
    }

    public void setTuner(boolean tuner) {
        this.tuner = tuner;
    }

    public double getLastRecordedFreq() {
        return lastRecordedFreq;
    }

    public void setLastRecordedFreq(double lastRecordedFreq) {
        this.lastRecordedFreq = lastRecordedFreq;
    }

    public byte getCleanTone() {
        return cleanTone;
    }

    public void setCleanTone(byte cleanTone) {
        this.cleanTone = cleanTone;
    }

    public boolean isEffect() {
        return effect;
    }

    public void setEffect(boolean effect) {
        this.effect = effect;
    }

    public void setEffect() {
        effect = true;
        tuner = false;
    }

    public byte getDistortionLevel() {
        return (byte) (distortionLevel / 2);
    }

    public void setDistortionLevel(byte distortionLevel) {
        this.distortionLevel = distortionLevel;
    }

    public byte getDistortionGain() {
        return (byte) (distortionGain / 20);
    }

    public void setDistortionGain(byte distortionGain) {
        this.distortionGain = distortionGain;
    }

    public byte getGain() {
        return (byte) (gain / 20);
    }

    public void setGain(byte gain) {
        this.gain = gain;
    }

    public RecordingStatus getRecordingStatus() {
        return recordingStatus;
    }

    public void setRecordingStatus(RecordingStatus recordingStatus) {
        this.recordingStatus = recordingStatus;
    }
}
