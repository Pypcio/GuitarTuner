package Controller;

import AudioService.AudioService;
import Enums.RecordingStatus;
import Enums.RecordingType;
import FrequencyService.FrequencyToNoteRangeDictionary;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import org.apache.log4j.Logger;

import javax.sound.sampled.LineUnavailableException;
import java.net.URL;
import java.util.HashSet;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * Created by Szymon on 2017-03-11.
 */
public class Controller implements Initializable {
    static Logger log = Logger.getLogger(Controller.class.getName());
    @FXML
    Button microOnBtn;
    @FXML
    Button microOffBtn;
    @FXML
    Button jackOnBtn;
    @FXML
    Button jackOffBtn;
    @FXML
    Circle centerCircle;
    @FXML
    Circle leftCircle0;
    @FXML
    Circle leftCircle1;
    @FXML
    Circle leftCircle2;
    @FXML
    Circle rightCircle0;
    @FXML
    Circle rightCircle1;
    @FXML
    Circle rightCircle2;
    @FXML
    TextField noteField;
    @FXML
    TextField frequencyField;
    @FXML
    TextField frequencyIdealField;
    @FXML
    ToggleButton distToggleBtn;
    @FXML
    ProgressIndicator leftProgress;
    @FXML
    ProgressIndicator centerProgress;
    @FXML
    ProgressIndicator rightProgress;
    @FXML
    Circle distBulb0;
    @FXML
    Circle distBulb1;
    private RecordingType recordingType = RecordingType.NONE;
    private FrequencyToNoteRangeDictionary frequencyToNoteRangeDictionary = new FrequencyToNoteRangeDictionary();
    private EffectConfigurator configurator = new EffectConfigurator(50, 50, 50);
    private AudioService audioService = new AudioService(configurator);


    Task audioMixer = new Task<Void>() {

        @Override
        protected Void call() throws Exception {
            audioService.liveMonitor();
            return null;
        }
    };

    Task audioRecorder = new Task<Void>() {

        @Override
        protected Void call() throws Exception {
            audioService.record();
            return null;
        }
    };

    private static class TimerService extends ScheduledService<Integer> {
        protected Task<Integer> createTask() {
            return new Task<Integer>() {
                protected Integer call() {
                    return 0;
                }
            };
        }
    }

    public Controller() throws LineUnavailableException {
    }

    public void initialize(URL location, ResourceBundle resources) {
        Thread audioTh = new Thread(audioMixer);
        audioTh.setDaemon(true);
        audioTh.start();
        Thread audioRec = new Thread(audioRecorder);
        audioRec.setDaemon(true);
        audioRec.start();

        TimerService service = new TimerService();
        service.setPeriod(Duration.seconds(1));
        service.setOnSucceeded(t -> {
            if (configurator.isTuner()) {
                if (RecordingStatus.SUCCESSED.equals(configurator.getRecordingStatus())) {
                    Optional<Double> freq = Optional.ofNullable(configurator.getLastRecordedFreq());
                    freq.ifPresent((x) -> setFrequency(x));
                    log.info("Calculated frequency: " + freq);
                    configurator.setRecordingStatus(RecordingStatus.READY);
                }
            }
        });
        service.start();
    }

    @FXML
    private void jackOff() throws InterruptedException {
        if (RecordingType.MICROPHONE.equals(recordingType)) {
            microOn();
        }
        configurator.setTuner(true);
        recordingType = RecordingType.JACK;
        jackOnBtn.setVisible(true);
        jackOffBtn.setVisible(false);
    }

    @FXML
    private void jackOn() {
        jackOnBtn.setVisible(false);
        jackOffBtn.setVisible(true);
        recordingOff();

    }

    @FXML
    public void microOff() {
        if (RecordingType.JACK.equals(recordingType)) {
            jackOn();
        }
        recordingType = RecordingType.MICROPHONE;
        microOnBtn.setVisible(true);
        microOffBtn.setVisible(false);
    }

    @FXML
    public void microOn() {
        microOnBtn.setVisible(false);
        microOffBtn.setVisible(true);
        recordingOff();
    }

    @FXML
    public void exit() {
        System.exit(0);
    }

    @FXML
    public void exitMain() {
        System.exit(0);
    }

    @FXML
    public void distBtn() {
        Set<Circle> bulbs = new HashSet();
        bulbs.add(distBulb0);
        bulbs.add(distBulb1);
        turnToggleBtn(distToggleBtn, bulbs);
    }

    @FXML
    public void leftMinusClicked() {
        progressDecrement(leftProgress);
        configurator.setGain((byte) (rightProgress.getProgress() * 100));
    }

    @FXML
    public void leftPlusClicked() {
        progressIncrement(leftProgress);
        configurator.setGain((byte) (rightProgress.getProgress() * 100));
    }

    @FXML
    public void centerMinusClicked() {
        progressDecrement(centerProgress);
        configurator.setDistortionLevel((byte) (rightProgress.getProgress() * 100));
    }

    @FXML
    public void centerPlusClicked() {
        progressIncrement(centerProgress);
        configurator.setDistortionLevel((byte) (rightProgress.getProgress() * 100));
    }

    @FXML
    public void rightMinusClicked() {
        progressDecrement(rightProgress);
        configurator.setDistortionGain((byte) (rightProgress.getProgress() * 100));
    }

    @FXML
    public void rightPlusClicked() {
        progressIncrement(rightProgress);
        configurator.setDistortionGain((byte) (rightProgress.getProgress() * 100));
    }

    private void turnToggleBtn(ToggleButton btn, Set<Circle> bulbs) {
        if (btn.isSelected()) {
            for (Circle circle : bulbs) {
                turnOnBulb(circle, ColorFactory.GREEN_COLOR);
                configurator.setEffect(true);
            }
        } else {
            for (Circle circle : bulbs) {
                turnOnBulb(circle, ColorFactory.RED_COLOR);
                configurator.setEffect(false);
            }
        }
    }

    private void recordingOff() {
        configurator.setTuner(false);
        recordingType = RecordingType.NONE;
        configurator.setEffect();
        clearFrequencyValues();
    }

    private void turnOnBulb(Circle bulb, Color color) {
        bulb.setFill(color);
    }

    private void turnOffBulb(Circle bulb) {
        turnOnBulb(bulb, ColorFactory.LIGHT_BROWN_COLOR);
    }

    private void clearFrequencyValues() {
        this.frequencyField.setText("");
        this.frequencyIdealField.setText("");
        clearBulbs();
        clearNoteValue();
    }

    private void clearNoteValue() {
        this.noteField.setText("");
    }

    private void setFrequency(double freq) {
        String freqByString = String.valueOf(freq);
        frequencyField.setText(freqByString);
        setNoteValue();
    }

    private void setNoteValue() {
        float freq = (Float.valueOf(this.frequencyField.getText()));
        String note = frequencyToNoteRangeDictionary.getNote(freq);
        this.noteField.setText(note);
        this.frequencyIdealField.setText(String.valueOf(frequencyToNoteRangeDictionary.idealFreqForNote(note)));
        setDeviationBulbs(frequencyToNoteRangeDictionary.setDeviationLevel(note, freq));
    }

    private void setDeviationBulbs(Optional<Integer> deviationByOptional) {
        if (deviationByOptional.isPresent()) {
            clearBulbs();
            int deviation = deviationByOptional.get();
            if (deviation == 0) {
                turnOnBulb(centerCircle, ColorFactory.GREEN_COLOR);
            } else if (deviation == -1) {
                turnOnBulb(leftCircle0, ColorFactory.RED_COLOR);
            } else if (deviation == -2) {
                turnOnBulb(leftCircle1, ColorFactory.RED_COLOR);
            } else if (deviation == -3) {
                turnOnBulb(leftCircle2, ColorFactory.RED_COLOR);
            } else if (deviation == 1) {
                turnOnBulb(rightCircle0, ColorFactory.RED_COLOR);
            } else if (deviation == 2) {
                turnOnBulb(rightCircle1, ColorFactory.RED_COLOR);
            } else if (deviation == 3) {
                turnOnBulb(rightCircle2, ColorFactory.RED_COLOR);
            }
        } else {
            clearFrequencyValues();
        }
    }

    private void clearBulbs() {
        turnOffBulb(centerCircle);
        turnOffBulb(leftCircle0);
        turnOffBulb(leftCircle1);
        turnOffBulb(leftCircle2);
        turnOffBulb(rightCircle0);
        turnOffBulb(rightCircle1);
        turnOffBulb(rightCircle2);
    }

    private void progressIncrement(ProgressIndicator indicator) {
        if (indicator.getProgress() < 0.99) {
            indicator.setProgress(indicator.getProgress() + 0.1);
        }
    }

    private void progressDecrement(ProgressIndicator indicator) {
        if (indicator.getProgress() > 0.01) {
            indicator.setProgress(indicator.getProgress() - 0.1);
        }
    }
}

