import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 * Created by Szymon on 2017-03-11.
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Logger log = Logger.getLogger(Main.class.getName());
        BasicConfigurator.configure();
        log.debug("Begin of application, loading view");
        Parent root = FXMLLoader.load(getClass().getResource("/view.fxml"));
        primaryStage.setTitle("");
        primaryStage.setScene(new Scene(root, 800, 480));
        primaryStage.show();
    }
}
