import FrequencyService.FrequencyToNoteRangeDictionary;
import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

/**
 * Created by Szymon Pypłacz on 2017-12-08.
 */
public class FrequencyToNoteRangeDictionaryTest {
    FrequencyToNoteRangeDictionary dictionary = new FrequencyToNoteRangeDictionary();


    @Test
    public void noteTest() {
        float frequency = 440.0f;
        String note = dictionary.getNote(frequency);
        Assert.assertEquals(note, "A4");
    }

    @Test
    public void deviationLevelTest(){
        float frequency = 445.0f;
        String note = dictionary.getNote(frequency);
        Optional<Integer> deviationLevel = dictionary.setDeviationLevel(note, frequency);
        Assert.assertEquals(note, "A4");
        Assert.assertEquals(deviationLevel, Optional.of(1));
    }

    @Test
    public void idealNoteTest(){
        String note = "A4";
        String note2 = "A5";
        String note3 = "A3";
        float[] idealFrequencies = {440,880,220};
        float[] frequency =  new float[3];
        frequency[0] = dictionary.idealFreqForNote(note);
        frequency[1] = dictionary.idealFreqForNote(note2);
        frequency[2] = dictionary.idealFreqForNote(note3);
        Assert.assertArrayEquals(idealFrequencies, frequency, 0);
    }
}
